/* eslint-disable */

import Api from '@/services/Api'

export default {
	getOrders() {
		return Api().get('orders')
	},
	getFacilities(store){
		return Api().get(`facility?store=${store}`)
	},
	removeOrder(id){
		return Api().delete(`orders?id=${id}`)
	}
}
