/* eslint-disable */

import Vue from 'vue';
import Router from 'vue-router';
import OrdersTable from '@/components/OrdersTable';
import BootstrapVue from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(Router);
Vue.use(BootstrapVue);

export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'OrdersTable',
			component: OrdersTable
		}
	]
})
